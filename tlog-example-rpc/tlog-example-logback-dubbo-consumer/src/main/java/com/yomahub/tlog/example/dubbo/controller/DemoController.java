package com.yomahub.tlog.example.dubbo.controller;

import com.yomahub.tlog.core.annotation.TLogAspect;
import com.yomahub.tlog.example.dubbo.service.DemoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class DemoController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DemoService demoService;

    @RequestMapping("/hi")
    @TLogAspect("name")
    public String sayHello(@RequestParam String name){
        log.info("logback-dubbo-consumer:invoke method sayHello,name={}",name);
        String str = demoService.sayHello(name);
        return str;
    }
}
